# fn.isInString must be included before

# working example
example = PAGE
example {
	10 = COA
	10 {
	        10 = TEXT
		10.value = Your content will apear, if the needle was found in the haystack! :O

		stdWrap.if.isTrue.cObject < fn.isInString
		stdWrap.if.isTrue.cObject.1 {
			needle = needle
			haystack = needle in the haystack
		}
	}
}
