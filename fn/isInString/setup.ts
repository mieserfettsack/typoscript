# fn.isInString will return true, if the needle was found in string
fn.isInString = COA
fn.isInString {
	1 = LOAD_REGISTER
	1 {
		# REGISTER for what you are looking for
		needle = 

		# REGISTER 
		haystack = 

		# split haystack by the needle
		# result is a haystack without a needle, if found
		findNeedleInHaystack.cObject = TEXT
		findNeedleInHaystack.cObject {
			data = REGISTER:haystack
			split.token.data = REGISTER:needle 
		}
	}

	# compare REGISTER:haystack and result of REGISTER:findNeedleInHaystack
	# if both are the same, nothing was found
	10 = TEXT
	10 {
		# true will be returned, if the needle was found inside haystack
		value = true

		if {
			value.data = REGISTER:haystack 
			equals.data = REGISTER:findNeedleInHaystack

			negate = 1
		}
	}
}
