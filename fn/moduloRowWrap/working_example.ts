﻿page >
# fn.moduloRowWrap must be included before

# define the select since we are using it two times
aSelectToRetriveSomeData = CONTENT
aSelectToRetriveSomeData {
	table = tt_content
	# make sure to use a page that exists and has at least 4 items
	select.pidInList = 1
}

exmaple = PAGE
exmaple {
	1 = LOAD_REGISTER
	# we need to know about the amount of items in the select to be wrapped with rows
	1.amountOfItems.numRows < aSelectToRetriveSomeData

	10 < aSelectToRetriveSomeData
	10 {		
		renderObj = COA
		renderObj {
			10 = TEXT
			10 {
				field = header
				
				stdWrap {
					required = 1
					wrap = <h3>|</h3>
				}
			}
			
			20 = TEXT
			20 {
				field = bodytext
				
				stdWrap {
					required = 1
					wrap = <p>|</p>
				}
			}
			
			stdWrap {
				required = 1
				wrap = <div class=item>|</div>

				# the content without the row wrap would look like this
				# <div class=item><h3>headline</h3><p>content</p></div>
				# <div class=item><h3>headline</h3><p>content</p></div>
				# <div class=item><h3>headline</h3><p>content</p></div>
				# <div class=item><h3>headline</h3><p>content</p></div>
				# ...
				
				# wrap3 is not used so copy fn.moduloRowWrap to it
				wrap3.cObject < fn.moduloRowWrap
				# fn.moduloRowWrap needs to know about the max amount of items
				wrap3.cObject.1.itemsMax.data = REGISTER:amountOfItems
				# we need a counting element. Note: Start counting at 1
				wrap3.cObject.1.itemsCounter.data = COBJ:parentRecordNumber
			}
		}
	}
}

# the final content will have rows
#	<div class=row>
#		<div class=item><h3>headline</h3><p>content</p></div>
#		<div class=item><h3>headline</h3><p>content</p></div>
#		<div class=item><h3>headline</h3><p>content</p></div>
#	</div>
#	<div class=row>
#		<div class=item><h3>headline</h3><p>content</p></div>
#	</div>
