﻿fn.moduloRowWrap = COA
fn.moduloRowWrap {
	1 = LOAD_REGISTER
	1 {
		# amount if items per row
		itemsPerRow.ifEmpty = 3

		# you must set itemsMax, otherwise the last row will not always be closed
		itemsMax = 
		# define a counting element when using fn.moduloRowWrap
		itemsCounter =

		# a row will be wrapped by <div class=row></div> by default
		rowClassName.ifEmpty = row
		rowTagName.ifEmpty = div

		# calculates the modulo per iteration
		moduloCalc {
			cObject = TEXT
			cObject {
				value = {REGISTER:itemsCounter}%{REGISTER:itemsPerRow}
				insertData = 1
			}
			prioriCalc = 1
		}

		# lastOrRowEnd will check if the current item is the end of line
		# or the very last item
		# it will return 1 if any condition mets
		lastOrRowEnd {
			cObject = COA
			cObject {
				10 = TEXT
				10 {
					if {
						value.data = REGISTER:moduloCalc 
						equals = 0
					}
					value = 1
				}

				20 = TEXT
				20 {
					if {
						value.data = REGISTER:itemsMax
						equals.data = REGISTER:itemsCounter 
					}
					value = 1
				}
			}
		}
	}

	# 10,20 and 30 will create a surrounding div per row
	
	# opens the row-wrap
	10 = TEXT
	10 {
		# each time the moduloCalc genrates 0 a new row just began
		if {
			value.data = REGISTER:moduloCalc 
			equals = 1
		}

		value = <{REGISTER:rowTagName} class="{REGISTER:rowClassName}">
		insertData = 1
	}

	# the pipe
	20 = TEXT
	20.value = |

	# close the row-wrap
	30 = TEXT
	30 {
		if.isTrue.data = REGISTER:lastOrRowEnd 

		value = </{REGISTER:rowTagName}>
		insertData = 1
	}
}
